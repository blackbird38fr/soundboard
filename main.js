
//playing the sound for the given keyCode
function play(keyCode){
    let $audio = $(`audio[data-key="${keyCode}"]`)[0];
   // console.log($audio);
    if ($audio != null){ 
        let $circle = $(`div[data-key="${keyCode}"]`)[0];
        createRipples($circle);
        $audio.currentTime = 0; 
        $audio.play();
    }else {
        console.log("You pressed a key that is not attached to any sound.");
    }
}

//ripples - sets and unsets the class 'active' for the clicked circle
function createRipples(circle){
    $(circle).addClass('active');
    setTimeout(function () {
        $(circle).removeClass('active');}
        , 800);
}

//https://learn.jquery.com/using-jquery-core/document-ready/
$(document).ready(function () {
    $(window).on('keydown', function playSound(event){
        play(event.keyCode);
    });

    //adding a listener for the circle clicks
    $('.key').each(function(key) {
        $(this).on('click', function(){ 
            //console.log(`${$( this ).text()} ${$( this )} ${$( this ).attr("data-key")}`); 
            play($( this ).attr("data-key"));
        });
    });
});
